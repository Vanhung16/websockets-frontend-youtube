import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// Services
import users from './services/UserService';
import sockets from './services/SocketService';

const app = createApp(App).use(router);

app.config.globalProperties.$users = users;
app.config.globalProperties.$sockets = sockets;

router.isReady().then(() => {
  app.mount('#app');
});
