import io from 'socket.io-client';

const socket = io(process.env.VUE_APP_API_URL);

socket.on('setID', async () => {
  console.log(socket.id)
  const user = JSON.parse(localStorage.getItem('user'));
  !user 
  ? null
  : (
    user.websocketID = socket.id,
    socket.emit('updateID', {email: user.email}),
    localStorage.setItem('user', JSON.stringify(user))
  );
});

export default socket
